$(document).ready(function(){

//hamburger

$('#hamburger-wrap').click(function(){
    $(this).find('#hamburger').toggleClass('open');
    $('body').toggleClass('menu-open');
    $('.menu').find('ul').removeClass('openForm');
    $('.menuform-container').removeClass('openForm');
})


    $('.slider').slick({
        nextArrow: '<div class="slick-arrow-right"><span></span></div>',
        prevArrow: '<div class="slick-arrow-left"><span></span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        // autoplay:true,

        responsive: [
            // {
            //   breakpoint: 1024,
            //   settings: {
            //     slidesToShow: 3,
            //     slidesToScroll: 3,
            //     infinite: true,
            //     dots: true
            //   }
            // },
            // {
            //   breakpoint: 600,
            //   settings: {
            //     slidesToShow: 2,
            //     slidesToScroll: 2
            //   }
            // },
            // {
            //   breakpoint: 480,
            //   settings: {
            //     slidesToShow: 1,
            //     slidesToScroll: 1
            //   }
            // }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]

    });


    //floating placeholder

    $('.input-row input, .input-row textarea').keyup(function() {
        $(this).parent().find('span').addClass('float');
            var input = $(this).val();
            if(input) $(this).parent().find('span').addClass('float');
        else $(this).parent().find('span').removeClass('float');
    });

    // $('.input-row').click(function() {
    //     $(this).find('span').addClass('float');
    //     var input = $(this).find('input').val();
    //     if(input) $(this).find('span').addClass('float');
    //     // else $(this).find('span').removeClass('float');
    //     // else $(this).find('span').removeClass('float');
    //     else $(this).mouseleave(function () {
    //         $(this).find('span').removeClass('float');
    //     })
    // });

    $('form button').on("click", function () {
        $('form').find('.input-row').removeClass('float');
    });

    // phone mask
    $(function() {
        $('[name="phone"]').mask("+7 (000) 000-00-00", {
            clearIfNotMatch: true,
            placeholder: ""
        });
        $('[name="phone"]').focus(function(e) {


            if ($('[name="phone"]').val().length == 0) {
                $(this).val('+7(');
                $(this).parent().find('span').addClass('float');
            }

        })
    });

// autoheight textarea
    (function($)
{
    /**
     * Auto-growing textareas; technique ripped from Facebook
     *
     *
     * http://github.com/jaz303/jquery-grab-bag/tree/master/javascripts/jquery.autogrow-textarea.js
     */
    $.fn.autogrow = function(options)
    {
        return this.filter('textarea').each(function()
        {
            var self         = this;
            var $self        = $(self);
            var minHeight    = $self.height();
            var noFlickerPad = $self.hasClass('autogrow-short') ? 0 : parseInt($self.css('lineHeight')) || 0;
            var settings = $.extend({
                preGrowCallback: null,
                postGrowCallback: null
              }, options );

            var shadow = $('<div></div>').css({
                position:    'absolute',
                top:         -10000,
                left:        -10000,
                width:       $self.width(),
                fontSize:    $self.css('fontSize'),
                fontFamily:  $self.css('fontFamily'),
                fontWeight:  $self.css('fontWeight'),
                lineHeight:  $self.css('lineHeight'),
                resize:      'none',
    			'word-wrap': 'break-word'
            }).appendTo(document.body);

            var update = function(event)
            {
                var times = function(string, number)
                {
                    for (var i=0, r=''; i<number; i++) r += string;
                    return r;
                };

                var val = self.value.replace(/&/g, '&amp;')
                                    .replace(/</g, '&lt;')
                                    .replace(/>/g, '&gt;')
                                    .replace(/\n$/, '<br/>&#xa0;')
                                    .replace(/\n/g, '<br/>')
                                    .replace(/ {2,}/g, function(space){ return times('&#xa0;', space.length - 1) + ' ' });

				// Did enter get pressed?  Resize in this keydown event so that the flicker doesn't occur.
				if (event && event.data && event.data.event === 'keydown' && event.keyCode === 13) {
					val += '<br />';
				}

                shadow.css('width', $self.width());
                shadow.html(val + (noFlickerPad === 0 ? '...' : '')); // Append '...' to resize pre-emptively.

                var newHeight=Math.max(shadow.height() + noFlickerPad, minHeight);
                if(settings.preGrowCallback!=null){
                  newHeight=settings.preGrowCallback($self,shadow,newHeight,minHeight);
                }

                $self.height(newHeight);

                if(settings.postGrowCallback!=null){
                  settings.postGrowCallback($self);
                }
            }

            $self.change(update).keyup(update).keydown({event:'keydown'},update);
            $(window).resize(update);

            update();
        });
    };
})(jQuery);

$(function() {
   $('.autogrow-short').autogrow();
});


$('.autogrow-short').css('overflow', 'hidden').autogrow()


// toggle form
$('.toggleForm').click(function(){
    $('.menu').find('ul').toggleClass('openForm');
    $('.menuform-container').toggleClass('openForm');
});
// toggle footer__form
$('#toggle-details').click(function(){
    $('.footer__form').toggleClass('open');
    $(this).toggleClass('open');
});
// toggle footer__form on mobile

    $(window).on('load resize', function() {
            if ($(window).width() < 992) {
                $("#toggle-details").addClass('toggle-detailsOnMob');
            } else {
                $("#toggle-details").removeClass('toggle-detailsOnMob');
            }

        });

        $(window).on('load', function() {
            $('.toggle-detailsOnMob').click(function() {
                $('.details-col').slideToggle('slow');
            });
        });

//formstyler
(function($) {
        $(function() {
            $('input, select').styler({

            });
        });
    })(jQuery);



});
